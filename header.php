<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title><?php ot_echo_option('site_title','Frame YSR'); ?> - <?php ot_echo_option('site_slogan','A HTML5 & CSS3 - Wordpress Theme'); ?></title>
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/style.css">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/lightbox.css">
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
	<!--[if lt IE 9]>
	<script src="dist/html5shiv.js"></script>
	<![endif]-->
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
	<header style="background: url(<?php ot_echo_option('header_image', get_template_directory_uri().'/img/banner.jpg'); ?>) center;">
		<div class="content">
			<h1 class="logo"><?php ot_echo_option('site_title','Frame YSR'); ?></h1>
			<a id="sayfa" class="button" href="#" title="<?php ot_echo_option('page_title','About Me') ?>"><?php ot_echo_option('page_title','About Me') ?></a>
		</div>
	</header>
	<aside> 
		<div class="content">
			<h3><?php ot_echo_option('page_title','About Me') ?></h3>
			<?php ot_echo_option('page_content','<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus, qui eos placeat beatae aperiam <strong>eligendi fuga numquam maiores</strong> voluptates officiis? Deserunt, <a href="#">perferendis esse voluptatum</a> facilis minima dignissimos explicabo quos illum.</p>
			<p>Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris sed pretium risus. Proin feugiat tellus ligula, quis fringilla leo eleifend sed. Vestibulum imperdiet dignissim lacus quis malesuada. Integer enim arcu, aliquet fermentum felis nec, malesuada posuere odio. </p>') ?>
		</div>
	</aside>