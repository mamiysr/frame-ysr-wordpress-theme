<?php
/**
 * Initialize the custom theme options.
 */
add_action( 'admin_init', 'custom_theme_options' );

/**
 * Build the custom settings & update OptionTree.
 */
function custom_theme_options() {
  
  /* OptionTree is not loaded yet */
  if ( ! function_exists( 'ot_settings_id' ) )
    return false;
    
  /**
   * Get a copy of the saved settings array. 
   */
  $saved_settings = get_option( ot_settings_id(), array() );
  
  /**
   * Custom settings array that will eventually be 
   * passes to the OptionTree Settings API Class.
   */
  $custom_settings = array( 
    'contextual_help' => array( 
      'sidebar'       => ''
    ),
    'sections'        => array( 
      array(
        'id'          => 'ustsayfa',
        'title'       => 'Üst Sayfa - Header Page'
      ),
      array(
        'id'          => 'sitebilgi',
        'title'       => 'Site Bilgileri - Site Infos'
      ),
      array(
        'id'          => 'footer',
        'title'       => 'Alt - Footer'
      )
    ),
    'settings'        => array( 
      array(
        'id'          => 'page_title',
        'label'       => 'Üst Sayfa Başlığı - Header Page Title',
        'desc'        => '',
        'std'         => '',
        'type'        => 'text',
        'section'     => 'ustsayfa',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),
      array(
        'id'          => 'page_content',
        'label'       => 'Üst Sayfa İçeriği - Header Page Content',
        'desc'        => '',
        'std'         => '',
        'type'        => 'textarea',
        'section'     => 'ustsayfa',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),
      array(
        'id'          => 'site_title',
        'label'       => 'Site Başlığı - Site Title',
        'desc'        => '',
        'std'         => '',
        'type'        => 'text',
        'section'     => 'sitebilgi',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),
      array(
        'id'          => 'site_slogan',
        'label'       => 'Site Slogan',
        'desc'        => '',
        'std'         => '',
        'type'        => 'text',
        'section'     => 'sitebilgi',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),
      array(
        'id'          => 'header_image',
        'label'       => 'Üst Resim - Header Image',
        'desc'        => '',
        'std'         => '',
        'type'        => 'upload',
        'section'     => 'sitebilgi',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),
      array(
        'id'          => 'index_content',
        'label'       => 'Kısa Bilgi  - Short Index Content',
        'desc'        => '',
        'std'         => '',
        'type'        => 'textarea',
        'section'     => 'sitebilgi',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),
      array(
        'id'          => 'portfolio_category',
        'label'       => 'Portföy Kategorisi - Portfolio Category',
        'desc'        => '',
        'std'         => '',
        'type'        => 'category-select',
        'section'     => 'sitebilgi',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),
      array(
        'id'          => 'footer_info',
        'label'       => 'Alt Bilgi - Footer info',
        'desc'        => '',
        'std'         => '',
        'type'        => 'text',
        'section'     => 'footer',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),
      array(
        'id'          => 'social_network',
        'label'       => 'Sosyal Ağlar - Social Networks',
        'desc'        => '',
        'std'         => '',
        'type'        => 'social-links',
        'section'     => 'footer',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      )
    )
  );
  
  /* allow settings to be filtered before saving */
  $custom_settings = apply_filters( ot_settings_id() . '_args', $custom_settings );
  
  /* settings are not the same update the DB */
  if ( $saved_settings !== $custom_settings ) {
    update_option( ot_settings_id(), $custom_settings ); 
  }
  
  /* Lets OptionTree know the UI Builder is being overridden */
  global $ot_has_custom_theme_options;
  $ot_has_custom_theme_options = true;
  
}