<?php get_header(); ?>
<div id="intro">
	<div class="content">
		<?php ot_echo_option('index_content','<h2>Hi there! This is Frame YSR, a responsive portfolio template</h2>
		<p>Built by <a href="http://www.medialoot.com/">Medialoot</a> using HTML5 &amp; CSS3</p>
		<p>Coded to Wordpress by <a href="http://www.mamiysr.com/">mamiYSR</a> using Option Tree </p>
		<a class="hireMe" href="mailto:myemail@mail.com">HIRE ME</a>') ?>
	</div>
</div>
<div id="gallery">
	<ul class="content">
	<?php 
		$cat = "cat=".ot_get_option('portfolio_category','1');
		$query = new WP_Query($cat);
		if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); 
		
		$cat = array(); 
		foreach((get_the_category()) as $category) { $cat[] = $category->cat_name; }
	?> 
		<li>
			<a href="<?php echo wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' )[0]; ?>" data-lightbox="image-1" data-title="<?php the_title(); ?> / <?php echo implode(', ', $cat); ?>">
				<?php the_post_thumbnail( array(300, 200) , array('class' => 'image', /* 'alt' => the_title() */ ) ); ?>
			</a>
		</li>
	<?php 
		endwhile; 
			wp_reset_postdata();
		else : 
	?>
		<p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
	<?php 
		endif; 
	?>
	</ul>
</div>
<?php get_footer(); ?>
