<footer>
	<div class="content">
		<div id="copyright"><?php ot_echo_option('footer_info','Copyright 2015. All Rights Reserved, Powered By Wordpress, Design By MediaLoot.'); ?></div>
		<div id="socials">
			<ul>
			<?php foreach( ot_get_option('social_network') as $social ): ?>
				<?php if($social['href'] != null): ?>
					<li>
						<a class="<?php echo strtolower($social['name']); ?>" href="<?php echo $social['href']; ?>">
							<?php $social['name']; ?>
						</a>
					</li>
				<?php endif; ?>
			<?php endforeach; ?>
			</ul>
		</div>
	</div>
</footer>
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery-1.11.0.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/lightbox.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/main.js"></script>
<?php wp_footer(); ?>
</body>
</html>