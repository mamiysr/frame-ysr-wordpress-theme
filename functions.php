<?php
/* OT Settings */
add_filter( 'ot_theme_mode', '__return_true' );
add_filter( 'ot_show_pages', '__return_false' );
add_filter( 'ot_show_options_ui', '__return_false' );
add_filter( 'ot_show_new_layout', '__return_false' );
add_filter( 'ot_allow_unfiltered_html', '__return_false' );

require( trailingslashit( get_template_directory() ) . 'option-tree/ot-loader.php' );
require( trailingslashit( get_template_directory() ) . 'option-tree/theme-options.php' );

/* WP Settings */
add_theme_support( 'post-thumbnails' );
